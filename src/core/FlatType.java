/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author admin
 */
public class FlatType extends Database{
    private static final String cols[] = {"flat_type_id", "flat_type_name", "flat_type_property_tax", "flat_type_maintenance_cost"};
    
    private static String primaryKey = "flat_type_id";
    private List<String> columnNames;
    private String tableName;
    
    private int flatTypeID;
    private String flatTypeName;
    private double flatTypePropertyTax;
    private double flatTypeMaintenanceCost;
    
    
    
    {
        tableName = "flat_type";
        columnNames = new ArrayList<>();
        for(String col: cols){
            columnNames.add(col);
        }
    }
    public FlatType() {
        
    }
    public FlatType(int flatTypeID, String flatTypeName, double flatTypePropertyTax, double flatTypeMaintenanceCost) {
        this.flatTypeID = flatTypeID;
        this.flatTypeName = flatTypeName;
        this.flatTypePropertyTax = flatTypePropertyTax;
        this.flatTypeMaintenanceCost = flatTypeMaintenanceCost;
    }
    //GETTERS AND SETTERS

    public int getFlatTypeID() {
        return flatTypeID;
    }

    public String getFlatTypeName() {
        return flatTypeName;
    }

    public void setFlatTypeName(String flatTypeName) {
        this.flatTypeName = flatTypeName;
    }

    public double getFlatTypePropertyTax() {
        return flatTypePropertyTax;
    }

    public void setFlatTypePropertyTax(double flatTypePropertyTax) {
        this.flatTypePropertyTax = flatTypePropertyTax;
    }

    public double getFlatTypeMaintenanceCost() {
        return flatTypeMaintenanceCost;
    }

    public void setFlatTypeMaintenanceCost(double flatTypeMaintenanceCost) {
        this.flatTypeMaintenanceCost = flatTypeMaintenanceCost;
    }
    
    
    
    //END OF GETTERS AND SETTERS
    
    //PUBLIC METHODS    
    public int insert(ArrayList<String> values){
        return Database.insert(columnNames, values, tableName);
    }
    
    public boolean update(ArrayList<String> columnNames, ArrayList<String> values, int id){
        return Database.update(columnNames, values, tableName, primaryKey, id);
    }
    
    public boolean delete(int id){
        return Database.delete(tableName, primaryKey, id);
    }
    
    public HashMap<Integer, FlatType>getAllFlatTypes(){
        try{
            return createHashMapFromResultSet(Database.select(tableName, null));
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, this.getClass() + " Something went Wrong while Fetching records!", "Error", JOptionPane.OK_OPTION);
            return null;
        }
    }
    
    public FlatType getFlatTypeByID(int id){
        try{
            return createHashMapFromResultSet(Database.select(tableName, "flat_type_id = " + id)).get(id);
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, this.getClass() + " Something went Wrong while Fetching records!", "Error", JOptionPane.OK_OPTION);
            return null;
        }
    }
    
    //END OF PUBLIC METHODS
    
    //PRIVATE FUNCTIONALITY
    private HashMap<Integer, FlatType> createHashMapFromResultSet(ResultSet rs) throws SQLException{
        HashMap<Integer, FlatType> flatTypeHashMap = new HashMap<>();
        //"flat_type_id", "flat_type_name", "flat_type_property_tax"
        while(rs.next()){
            int tempFlatTypeID                  =   rs.getInt("flat_type_id");
            String tempFlatTypeName             =   rs.getString("flat_type_name");
            double tempFlatTpePropertyTax       =   rs.getDouble("flat_type_property_tax");
            double tempFlatTypeMaintenanceCost  =   rs.getDouble("flat_type_maintenance_cost");
            flatTypeHashMap.put(tempFlatTypeID, new FlatType(tempFlatTypeID, tempFlatTypeName, tempFlatTpePropertyTax, tempFlatTypeMaintenanceCost));
        }
        return flatTypeHashMap;
    }
    
    //END OF PRIVATE 
    
    public static void main(String args[]){
        FlatType ft = new FlatType();
        ArrayList<String> values = new ArrayList<>();
        values.add("4 BHK");
        values.add("7500");
        values.add("2000");
        ft.insert(values);
        ArrayList<String> columnNames = new ArrayList<>();
        columnNames.add("flat_type_property_tax");
        values.add("5250");
        //ft.update(columnNames, values, "flat_type_id", 2);
        
//        ft.delete(2);
        HashMap<Integer, FlatType> hm = ft.getAllFlatTypes();
        for(int i: hm.keySet()){
            System.out.println("Flat Type Name: " + hm.get(i).getFlatTypeName());
            System.out.println("Flat Property Tax: "+hm.get(i).getFlatTypePropertyTax());
            System.out.println("Flat Maintenance Cost: "+hm.get(i).getFlatTypeMaintenanceCost());
        }
    }
}
