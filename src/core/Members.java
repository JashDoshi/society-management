package core;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Himanshu
 */
public class Members extends Database{
    private static final String cols[] = {"member_id", "name", "email_id", "gender", "contact_no", "religion", "profession", "flat_no", "flat_type_id"};
    
    private static String primaryKey = "member_id";
    private List<String> columnNames;
    private String tableName;
    
    private int memberID;
    private String name;
    private String emailID;
    private String gender;
    private String contactNo;
    private byte[] photo;
    private String religion;
    private String profession;
    private int flatNo;
    private int flatTypeID;
    
    
    {
        tableName = "members";
        columnNames = new ArrayList<>();
        for(String col: cols){
            columnNames.add(col);
        }
    }
    public Members() {
        
    }

    public Members(int memberID, String name, String emailID, String gender, String contactNo, byte[] photo, String religion, String profession, int flatNo, int flatTypeID) {
        this.memberID = memberID;
        this.name = name;
        this.emailID = emailID;
        this.gender = gender;
        this.contactNo = contactNo;
        this.photo = photo;
        this.religion = religion;
        this.profession = profession;
        this.flatNo = flatNo;
        this.flatTypeID = flatTypeID;
    }
    
    
    //GETTERS AND SETTERS

    public int getMemberID() {
        return memberID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public int getFlatNo() {
        return flatNo;
    }

    public void setFlatNo(int flatNo) {
        this.flatNo = flatNo;
    }

    public int getFlatTypeID() {
        return flatTypeID;
    }

    
    
    //END OF GETTERS AND SETTERS
    
    //PUBLIC METHODS    
    public int insert(ArrayList<String> values){
        return Database.insert(columnNames, values, tableName);
    }
    
    public boolean updatePhoto(byte[] image, int id){
        String sql = "UPDATE "+tableName + " SET photo = ? WHERE " + primaryKey + " = "+id;
        try{
            PreparedStatement ps = MySQLConnect.connectDB().prepareStatement(sql);
            ps.setBytes(1, image);
            int numRows = ps.executeUpdate();
            if(numRows>0){
                return true;
            }else{
                return false;
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Exception : " + e, "Error", JOptionPane.OK_OPTION);
            return false;
        }
    }
    public boolean update(ArrayList<String> columnNames, ArrayList<String> values, int id){
        return Database.update(columnNames, values, tableName, primaryKey, id);
    }
    
    public boolean delete(int id){
        return Database.delete(tableName, primaryKey, id);
    }
    
    public HashMap<Integer, Members>getAllMembers(){
        try{
            return createHashMapFromResultSet(Database.select(tableName, null));
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, this.getClass() + " Something went Wrong while Fetching records!", "Error", JOptionPane.OK_OPTION);
            return null;
        }
    }
    public Members getMemberByID(int id){
        try{
            return createHashMapFromResultSet(Database.select(tableName, primaryKey+ " = " + id)).get(id);
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, this.getClass() + " Something went Wrong while Fetching records!", "Error", JOptionPane.OK_OPTION);
            return null;
        }
    }
    //END OF PUBLIC METHODS
    
    //PRIVATE FUNCTIONALITY
    private HashMap<Integer, Members> createHashMapFromResultSet(ResultSet rs) throws SQLException{
        HashMap<Integer, Members> flatTypeHashMap = new HashMap<>();
        while(rs.next()){
            int tempMembersID                  =   rs.getInt("member_id");
            String tempName                     =   rs.getString("name");
            String tempEmailID                  =   rs.getString("email_id");
            String tempGender               =   rs.getString("gender");
            String tempContactNo            =   rs.getString("contact_no");
            byte b[]                    =   rs.getBytes("photo");
            String tempReligion         =   rs.getString("religion");
            String tempProfession       =   rs.getString("profession");
            int tempFlatNo              =   rs.getInt("flat_no");
            int tempFlatTypeID          =   rs.getInt("flat_type_id");
            flatTypeHashMap.put(tempMembersID, new Members(tempMembersID, tempName, tempEmailID, tempGender, tempContactNo, b, tempReligion, tempProfession, tempFlatNo, tempFlatTypeID));
        }
        return flatTypeHashMap;
    }
    
    //END OF PRIVATE 
    
    public static void main(String args[]){
        Members member = new Members();
        
        ArrayList<String> values = new ArrayList<>();
//        values.add("Krishna");
//        values.add("krishna@gmail.com");
//        values.add("Male");
//        values.add("98219098190");
//        values.add("Maratha");
//        values.add("Engineer");
//        values.add("401");
//        values.add("4");
        //member.insert(values);
        
        values.add("9800098000");
        values.add("404");
        ArrayList<String> columnNames = new ArrayList<>();
        columnNames.add("contact_no");
        columnNames.add("flat_no");
        //member.update(columnNames, values, 3);
        //member.delete(2);
        FlatType ft = new FlatType();
        HashMap<Integer, Members> hm = member.getAllMembers();
        for(int i: hm.keySet()){
            System.out.println("Member ID: " + hm.get(i).getMemberID());
            System.out.println("Name: " + hm.get(i).getName());
            System.out.println("Email ID : " + hm.get(i).getEmailID());
            System.out.println("Gender: " + hm.get(i).getGender());
            System.out.println("Contact No: " + hm.get(i).getContactNo());
            System.out.println("Religion: "+hm.get(i).getReligion());
            System.out.println("Profession : "+ hm.get(i).getProfession());
            System.out.println("Flat No: " + hm.get(i).getFlatNo());
            System.out.println("Flat Type ID: " + ft.getFlatTypeByID(hm.get(i).getFlatTypeID()).getFlatTypeName());
        }
    }
}
