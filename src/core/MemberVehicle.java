package core;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;

public class MemberVehicle extends Database{
    private static final String cols[] = {"member_vehicle_id", "member_id", "vehicle_type_id", "vehicle_number"};
    
    private static String primaryKey = "member_vehicle_id";
    private List<String> columnNames;
    private String tableName;
    
    private int memberVehicleID;
    private int memberID;
    private int vehicleTypeID;
    private String vehicleNumber;
    
    {
        tableName = "member_vehicle";
        columnNames = new ArrayList<>();
        for(String col: cols){
            columnNames.add(col);
        }
    }

    public MemberVehicle() {
        
    }

    public MemberVehicle(int memberVehicleID, int memberID, int vehicleTypeID, String vehicleNumber) {
        this.memberVehicleID = memberVehicleID;
        this.memberID = memberID;
        this.vehicleTypeID = vehicleTypeID;
        this.vehicleNumber = vehicleNumber;
    }

    public int getMemberVehicleID() {
        return memberVehicleID;
    }

    public int getMemberID() {
        return memberID;
    }


    public int getVehicleTypeID() {
        return vehicleTypeID;
    }


    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }
    
    //PUBLIC METHODS    
    public int insert(ArrayList<String> values){
        return Database.insert(columnNames, values, tableName);
    }
    
    public boolean update(ArrayList<String> columnNames, ArrayList<String> values, int id){
        return Database.update(columnNames, values, tableName, primaryKey, id);
    }
    
    public boolean delete(int id){
        return Database.delete(tableName, primaryKey, id);
    }
    
    public HashMap<Integer, MemberVehicle>getAllMemberVehicles(){
        try{
            return createHashMapFromResultSet(Database.select(tableName, null));
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, this.getClass() + " Something went Wrong while Fetching records!", "Error", JOptionPane.OK_OPTION);
            return null;
        }
    }
    
    public MemberVehicle getMemberVehicleByID(int id){
        try{
            return createHashMapFromResultSet(Database.select(tableName, primaryKey+ " = " + id)).get(id);
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, this.getClass() + " Something went Wrong while Fetching records!", "Error", JOptionPane.OK_OPTION);
            return null;
        }
    }
    
    private HashMap<Integer, MemberVehicle> createHashMapFromResultSet(ResultSet rs) throws SQLException{
        HashMap<Integer, MemberVehicle> memberVehicleHashMap = new HashMap<>();
        
        while(rs.next()){
            int tempMemberVehicleID                  =   rs.getInt("member_vehicle_id");
            int tempMemberID                  =   rs.getInt("member_id");
            int tempVehicleTypeID                  =   rs.getInt("vehicle_type_id");
            String tempVehicleNumber       =   rs.getString("vehicle_number");

            memberVehicleHashMap.put(tempMemberVehicleID, new MemberVehicle(tempMemberVehicleID, tempMemberID, tempVehicleTypeID, tempVehicleNumber));
        }
        return memberVehicleHashMap;
    }
    
}
