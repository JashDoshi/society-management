package core;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;

public class VehicleType extends Database{
    private static final String cols[] = {"vehicle_type_id", "vehicle_type_name", "vehicle_typecharges"};
    
    private static String primaryKey = "vehicle_type_id";
    private List<String> columnNames;
    private String tableName;
    
    private int vehicleTypeID;
    private String vehicleTypeName;
    private double vehicleTypeCharges;
    
    {
        tableName = "vehicle_type";
        columnNames = new ArrayList<>();
        for(String col: cols){
            columnNames.add(col);
        }
    }

    public VehicleType() {
    }

    public VehicleType(int vehicleTypeID, String vehicleTypeName, double vehicleTypeCharges) {
        this.vehicleTypeID = vehicleTypeID;
        this.vehicleTypeName = vehicleTypeName;
        this.vehicleTypeCharges = vehicleTypeCharges;
    }

    public int getVehicleTypeID() {
        return vehicleTypeID;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public double getVehicleTypeCharges() {
        return vehicleTypeCharges;
    }

    public void setVehicleTypeCharges(double vehicleTypeCharges) {
        this.vehicleTypeCharges = vehicleTypeCharges;
    }
    
    
    //PUBLIC METHODS    
    public int insert(ArrayList<String> values){
        return Database.insert(columnNames, values, tableName);
    }
    
    public boolean update(ArrayList<String> columnNames, ArrayList<String> values, int id){
        return Database.update(columnNames, values, tableName, primaryKey, id);
    }
    
    public boolean delete(int id){
        return Database.delete(tableName, primaryKey, id);
    }
    
    public HashMap<Integer, VehicleType>getAllVehicleTypes(){
        try{
            return createHashMapFromResultSet(Database.select(tableName, null));
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, this.getClass() + " Something went Wrong while Fetching records!", "Error", JOptionPane.OK_OPTION);
            return null;
        }
    }
    
    public VehicleType getVehicleTypeByID(int id){
        try{
            return createHashMapFromResultSet(Database.select(tableName, "vehicle_type_id = " + id)).get(id);
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, this.getClass() + " Something went Wrong while Fetching records!", "Error", JOptionPane.OK_OPTION);
            return null;
        }
    }
    
    private HashMap<Integer, VehicleType> createHashMapFromResultSet(ResultSet rs) throws SQLException{
        HashMap<Integer, VehicleType> vehicleTypeHashMap = new HashMap<>();
        
        while(rs.next()){
            int tempVehicleTypeID                  =   rs.getInt("vehicle_type_id");
            String tempVehicleTypeName             =   rs.getString("vehicle_type_name");
            double tempVehicleTypeCharges       =   rs.getDouble("vehicle_type_charges");

            vehicleTypeHashMap.put(tempVehicleTypeID, new VehicleType(tempVehicleTypeID, tempVehicleTypeName, tempVehicleTypeCharges));
        }
        return vehicleTypeHashMap;
    }
    
}
