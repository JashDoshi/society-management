/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Himanshu
 */
public class Payment {
    private static final String cols[] = {"payment_id","bill_no", "payment_date", "payment_mode","payment_amount","cheque_no","cheque_date","bank_name"};
    
    private static String primaryKey = "payment_id";
    private List<String> columnNames;
    private String tableName;
    
    private int paymentID;
    private int billNo;
    private String paymentDate;
    private String paymentMode;
    private double paymentAmount;
    private String chequeNo;
    private String chequeDate;
    private String bankName;
    
    {
        tableName = "pament";
        columnNames = new ArrayList<>();
        for(String col: cols){
            columnNames.add(col);
        }
    }

    public Payment() {
    }

    public Payment(int paymentID, int billNo, String paymentDate, String paymentMode, double paymentAmount, String chequeNo, String chequeDate, String bankName) {
        this.paymentID = paymentID;
        this.billNo = billNo;
        this.paymentDate = paymentDate;
        this.paymentMode = paymentMode;
        this.paymentAmount = paymentAmount;
        this.chequeNo = chequeNo;
        this.chequeDate = chequeDate;
        this.bankName = bankName;
    }
    
    //GETTERS AND SETTERS

    public int getPaymentID() {
        return paymentID;
    }

    public int getBillNo() {
        return billNo;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getChequeDate() {
        return chequeDate;
    }

    public void setChequeDate(String chequeDate) {
        this.chequeDate = chequeDate;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
    
    //END OF GETTERS AND SETTERS
    
    //PUBLIC METHODS    
    
    public int insert(ArrayList<String> values){
        return Database.insert(columnNames, values, tableName);
    }
    
    public boolean update(ArrayList<String> columnNames, ArrayList<String> values, int id){
        return Database.update(columnNames, values, tableName, primaryKey, id);
    }
    
    public boolean delete(int id){
        return Database.delete(tableName, primaryKey, id);
    }
    
    public HashMap<Integer, Payment>getAllPayments(){
        try{
            return createHashMapFromResultSet(Database.select(tableName, null));
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, this.getClass() + " Something went Wrong while Fetching records!", "Error", JOptionPane.OK_OPTION);
            return null;
        }
    }
    
    public Payment getPaymentByID(int id){
        try{
            return createHashMapFromResultSet(Database.select(tableName, primaryKey + " = " + id)).get(id);
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, this.getClass() + " Something went Wrong while Fetching records!", "Error", JOptionPane.OK_OPTION);
            return null;
        }
    }
    
    //END OF PUBLIC METHODS
    
    //PRIVATE FUNCTIONALITY
    private HashMap<Integer, Payment> createHashMapFromResultSet(ResultSet rs) throws SQLException{
        HashMap<Integer, Payment> paymentHashMap = new HashMap<>();
        //"payment_id", "bill_no", "payment_date", "payment_mode","payment_amount","cheque_no","cheque_date","bank_name"
        while(rs.next()){
            int tempPaymentID           =   rs.getInt("payment_id");
            int tempBillNo              =   rs.getInt("bill_no");
            String tempPaymentDate      =   rs.getString("payment_date");
            String tempPaymentMode      =   rs.getString("payment_mode");
            double tempPaymentAmount        =   rs.getDouble("payment_amount");
            String tempChequeNo         =   rs.getString("cheque_no");
            String tempChequeDate       =   rs.getString("cheque_date");
            String tempBankName         =   rs.getString("bank_name");
            
            paymentHashMap.put(tempPaymentID, new Payment(tempPaymentID, tempBillNo, tempPaymentDate, tempPaymentMode, tempPaymentAmount, tempChequeNo, tempChequeDate, tempBankName));
        }
        return paymentHashMap;
    }
    
}
