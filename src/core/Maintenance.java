/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.swing.JOptionPane;

/**
 *
 * @author Himanshu
 */
public class Maintenance {
    private static final String cols[] = {"bill_no", "flat_no", "service_charges", "sinking_charges", "repair_funds", "festival_funds", "property_tax", "maintenance_cost", "parking_charges", "bill_date", "bill_due_date", "bill_status"};
    
    private static String primaryKey = "bill_no";
    private List<String> columnNames;
    private String tableName;
    
    private int billNo;
    private int flatNo;
    private double serviceCharges;
    private double sinkingCharges;
    private double repairFunds;
    private double festivalFunds;
    private double propertyTax;
    private double maintenanceCost;
    private double parkingCharges;
    private String billDate;
    private String billDueDate;
    private String billStatus;
    
    {
        tableName = "maintenance";
        columnNames = new ArrayList<>();
        for(String col: cols){
            columnNames.add(col);
        }
    }

    public Maintenance() {
    }

    public Maintenance(int billNo, int flatNo, double serviceCharges, double sinkingCharges, double repairFunds, double festivalFunds, double propertyTax, double maintenanceCost, double parkingCharges, String billDate, String billDueDate, String billStatus) {
        this.billNo = billNo;
        this.flatNo = flatNo;
        this.serviceCharges = serviceCharges;
        this.sinkingCharges = sinkingCharges;
        this.repairFunds = repairFunds;
        this.festivalFunds = festivalFunds;
        this.propertyTax = propertyTax;
        this.maintenanceCost = maintenanceCost;
        this.parkingCharges = parkingCharges;
        this.billDate = billDate;
        this.billDueDate = billDueDate;
        this.billStatus = billStatus;
    }
    
    //GETTERS AND SETTERS

    public int getBillNo() {
        return billNo;
    }
    
    public int getFlatNo() {
        return flatNo;
    }

    public double getServiceCharges() {
        return serviceCharges;
    }

    public void setServiceCharges(double serviceCharges) {
        this.serviceCharges = serviceCharges;
    }

    public double getSinkingCharges() {
        return sinkingCharges;
    }

    public void setSinkingCharges(double sinkingCharges) {
        this.sinkingCharges = sinkingCharges;
    }

    public double getRepairFunds() {
        return repairFunds;
    }

    public void setRepairFunds(double repairFunds) {
        this.repairFunds = repairFunds;
    }

    public double getFestivalFunds() {
        return festivalFunds;
    }

    public void setFestivalFunds(double festivalFunds) {
        this.festivalFunds = festivalFunds;
    }

    public double getPropertyTax() {
        return propertyTax;
    }

    public void setPropertyTax(double propertyTax) {
        this.propertyTax = propertyTax;
    }

    public double getMaintenanceCost() {
        return maintenanceCost;
    }

    public void setMaintenanceCost(double maintenanceCost) {
        this.maintenanceCost = maintenanceCost;
    }

    public double getParkingCharges() {
        return parkingCharges;
    }

    public void setParkingCharges(double parkingCharges) {
        this.parkingCharges = parkingCharges;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getBillDueDate() {
        return billDueDate;
    }

    public void setBillDueDate(String billDueDate) {
        this.billDueDate = billDueDate;
    }

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }
    
    
    
    //END OF GETTERS AND SETTERS
    
    public int insert(ArrayList<String> values){
        return Database.insert(columnNames, values, tableName);
    }
    
    public boolean update(ArrayList<String> columnNames, ArrayList<String> values, int id){
        return Database.update(columnNames, values, tableName, primaryKey, id);
    }
    
    public boolean delete(int id){
        return Database.delete(tableName, primaryKey, id);
    }
    
    public HashMap<Integer, Maintenance>getAllMaintenances(){
        try{
            return createHashMapFromResultSet(Database.select(tableName, null));
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, this.getClass() + " Something went Wrong while Fetching records!", "Error", JOptionPane.OK_OPTION);
            return null;
        }
    }
    
    public Maintenance getMaintenanceByID(int id){
        try{
            return createHashMapFromResultSet(Database.select(tableName, primaryKey+" = " + id)).get(id);
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, this.getClass() + " Something went Wrong while Fetching records!", "Error", JOptionPane.OK_OPTION);
            return null;
        }
    }
    public double getTotalMaintenanceAmount(){
        return this.festivalFunds+this.maintenanceCost+this.parkingCharges +this.propertyTax + this.repairFunds + this.serviceCharges + this.sinkingCharges;
    }
    public double getPenaltyAmountByID(int id){
        try{
            double penaltyAmount=0.00;
            Maintenance currentMaintenance = getMaintenanceByID(id);
            String billDueDate = currentMaintenance.getBillDueDate();
            Date currentDate = new Date();
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
            currentDate = sdf.parse(sdf.format(currentDate));
            
            Date dueDate = sdf.parse(billDueDate);
            if(currentDate.after(dueDate)){
                long diff = currentDate.getTime() - dueDate.getTime();
                float days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println("days = " + days);
                double amount =  currentMaintenance.getTotalMaintenanceAmount();
                
                penaltyAmount = (amount * 0.05)/days;
                System.out.println(penaltyAmount);
            }
            return Math.ceil(penaltyAmount);
        }catch(ParseException e){
            System.out.println("Date Parse Exception");
            return -1;
        }
        
    }
    //END OF PUBLIC METHODS
    
    //PRIVATE FUNCTIONALITY
    private HashMap<Integer, Maintenance> createHashMapFromResultSet(ResultSet rs) throws SQLException{
        HashMap<Integer, Maintenance> maintenanceHashMap = new HashMap<>();
        //"bill_no", "flat_no", "service_charges", "sinking_charges", "repair_funds", "festival_funds", "property_tax", "maintenance_cost", "parking_charges"
        while(rs.next()){
            int tempBillNo              =   rs.getInt("bill_no");
            int tempFlatNo              =   rs.getInt("flat_no");
            double tempServiceCharges   =   rs.getDouble("service_charges");
            double tempSinkingCharges   =   rs.getDouble("sinking_charges");
            double tempRepairFunds      =   rs.getDouble("repair_funds");
            double tempFestivalFunds    =   rs.getDouble("festival_funds");
            double tempPropertyTax      =   rs.getDouble("property_tax");
            double tempMaintenanceCost  =   rs.getDouble("maintenance_cost");
            double tempParkingCharges   =   rs.getDouble("parking_charges");
            String tempBillDate         =   rs.getString("bill_date");
            String tempBillDueDate      =   rs.getString("bill_due_date");
            String tempBillStatus       =   rs.getString("bill_status");
            maintenanceHashMap.put(tempBillNo, new Maintenance(tempBillNo, tempFlatNo, tempServiceCharges, tempSinkingCharges, tempRepairFunds, tempFestivalFunds, tempPropertyTax, tempMaintenanceCost, tempParkingCharges, tempBillDate, tempBillDueDate, tempBillStatus));
        }
        return maintenanceHashMap;
    }
    
    public static void main(String args[]){
        Maintenance m = new Maintenance();
        double penalty = m.getPenaltyAmountByID(1);
        System.out.println("Penalty = " + penalty);
    }
}
