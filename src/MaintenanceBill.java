/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ashay
 */
import java.io.FileOutputStream;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
public class MaintenanceBill {
   
    String dueDate;
    String owner;
    String flatNo;
    String sysDate;
    String serviceAmt;
    String sinkingAmt;
    String repairAmt;
    String maintenanceAmt;
    String festAmt; 
    String propertyAmt; 
    String parkingAmt;
    int total;
    private static String FILE = "c:/temp/maintenence.pdf";
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD);
    private static Font normalFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.NORMAL);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.BOLD);
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.BOLD);
    MaintenanceBill(String serviceAmt, String sinkingAmt,String repairAmt,String maintenanceAmt, String festAmt, String propertyAmt, String parkingAmt, String dueDate,String sysDate, String owner , String flatNo,int total){
        try {
            this.serviceAmt = serviceAmt;
            this.sinkingAmt =sinkingAmt;
            this.repairAmt = repairAmt;
            this.maintenanceAmt = maintenanceAmt;
            this. festAmt = festAmt;
            this.propertyAmt= propertyAmt;
            this.parkingAmt = parkingAmt;
            
            this.dueDate = dueDate;
            this.owner = owner;
            this.flatNo = flatNo;
           this.sysDate=sysDate;
            this.total = total;
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            addMetaData(document);
            addTitlePage(document);
            addContent(document);
            addFooterPage(document);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

     public void addMetaData(Document document) {
        document.addTitle("Maintenance Bill");
    }
     public void addTitlePage(Document document)throws DocumentException {
        Paragraph preface = new Paragraph();
        
        addEmptyLine(preface, 1);

        Paragraph p=new Paragraph();
        p.add(new Paragraph("       ANANDI GOPAL CO-OP HOUSING SOCIETY LTD",catFont));
        document.add(p);
        
        preface.add(new Paragraph("       regd No -TNA/KLN/HSG-11660/99-2000 KALYAN(W) 421 301",subFont));
        preface.add(new Paragraph("______________________________________________________________________________"));
        addEmptyLine(preface,1);
        addTab(preface,6);
        preface.add(("            Date:"+sysDate));
        addTab(preface,7);
        preface.add("             Due date: "+(dueDate));
        addEmptyLine(preface,1);
        preface.add(new Paragraph("Owner name: "+(owner),smallBold));
        preface.add(new Paragraph("FLAT NO: "+(flatNo),smallBold));

        addEmptyLine(preface,1);

        
        document.add(preface);
    } 
     public void addContent(Document document) throws DocumentException {

        Paragraph subPara = new Paragraph();
        // add a table
        createTable(subPara);
        document.add(subPara);

    }
     public void createTable(Paragraph subPara)
            throws BadElementException {
        float[] columnWidths= {5,1};
        PdfPTable table = new PdfPTable(columnWidths);

        try{
            PdfPCell c1 = new PdfPCell(new Phrase("Particulars",smallBold));
            c1.setHorizontalAlignment(0);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Amount",smallBold ));
            c1.setHorizontalAlignment(0);
            table.addCell(c1);

                table.addCell("Service charges");
                table.addCell(serviceAmt+"");
                table.addCell("Sinking charges");
                table.addCell(sinkingAmt+"");
                table.addCell("Repair charges");
                table.addCell(repairAmt+"");
                table.addCell("Maintenance charges");
                table.addCell(maintenanceAmt+"");
                table.addCell("Festival funds");
                table.addCell(festAmt+"");
                table.addCell("Property charges");
                table.addCell(propertyAmt+"");
                table.addCell("Parking charges");
                table.addCell(parkingAmt+"");
        
        }catch(Exception e){}
        
        float[] columnWidth= {5,1};
        PdfPTable table2 = new PdfPTable(columnWidth);
        try{
            
            PdfPCell c1 = new PdfPCell(new Phrase("GRAND TOTAL",subFont));
            table.addCell(c1);
            c1 = new PdfPCell(new Phrase(""+total,smallBold ));
            c1.setHorizontalAlignment(0);
            table.addCell(c1);
            
        }catch(Exception e){}
        subPara.add(table);

    }
     public static void addFooterPage(Document document)throws DocumentException {
        Paragraph preface = new Paragraph();
        
        addEmptyLine(preface, 1);

        preface.add(new Paragraph("Note:     1) 21% interests payable if payment not made by DUE DATE.",subFont));
        preface.add(new Paragraph("               2) Please pay by CHEQUE only.",subFont));
        preface.add(new Paragraph("               3) Please clear previous dues if any. ",subFont));
        document.add(preface);
    } 
    public static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
    public static void addTab(Paragraph paragraph,int number){
        String tab="    ";
        Chunk chunk=new Chunk("");
        for(int i=0;i<number;i++){
            chunk.append(tab);
            paragraph.add(chunk);
        }
    }

}