/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ashay
 */
import java.io.FileOutputStream;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
public class RecieptPDF {
   
    String date;
    String owner;
    String flatNo;
    String billNo;
    String paidBy;
    String total;
  
    private static String FILE = "c:/temp/reciept.pdf";
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD);
    private static Font normalFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.NORMAL);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.BOLD);
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.BOLD);
    //date,owner,flatNo,billNo,paidBy,total;
    RecieptPDF(String date, String owner, String flatNo, String billNo ,String paidBy,String total){
        try {
            this.date = date;
            this.owner = owner;
            this.flatNo = flatNo;
            this.billNo = billNo;
            this.paidBy = paidBy;
            this.total= total;  
 
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            addMetaData(document);
            addTitlePage(document);
            addContent(document);
            addFooterPage(document);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

     public void addMetaData(Document document) {
        document.addTitle("Reciept");
    }
     public void addTitlePage(Document document)throws DocumentException {
        Paragraph preface = new Paragraph();
        
        addEmptyLine(preface, 1);

        Paragraph p=new Paragraph();
        p.add(new Paragraph("       ANANDI GOPAL CO-OP HOUSING SOCIETY LTD",catFont));
        document.add(p);
        
        preface.add(new Paragraph("       regd No -TNA/KLN/HSG-11660/99-2000 KALYAN(W) 421 301",subFont));
        preface.add(new Paragraph("______________________________________________________________________________"));
        addEmptyLine(preface,1);
        
        preface.add(new Paragraph("                                                RECIEPT",catFont));
        addEmptyLine(preface,1);
        preface.add(new Paragraph("BILL NO:"+billNo,smallBold));
        addTab(preface,6);
        preface.add(("                     Date:"+date));
//        addTab(preface,6);
//        preface.add("             Due date: "+(dueDate));
        addEmptyLine(preface,1);
        preface.add(new Paragraph("Owner name: "+(owner),smallBold));
        preface.add(new Paragraph("FLAT NO: "+(flatNo),smallBold));

        addEmptyLine(preface,1);
      
        document.add(preface);
    } 
     public void addContent(Document document) throws DocumentException {

        Paragraph subPara = new Paragraph();
        // add a table
        createTable(subPara);
        document.add(subPara);

    }
    public void createTable(Paragraph subPara)
            throws BadElementException {
        float[] columnWidths= {5,1};
        PdfPTable table = new PdfPTable(columnWidths);

        try{
            PdfPCell c1 = new PdfPCell(new Phrase("",smallBold));
            c1.setHorizontalAlignment(0);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Amount",smallBold ));
            c1.setHorizontalAlignment(0);
            table.addCell(c1);
        
        }catch(Exception e){}
        
        float[] columnWidth= {5,1};
        PdfPTable table2 = new PdfPTable(columnWidth);
        try{
            
            PdfPCell c1 = new PdfPCell(new Phrase("TOTAL",subFont));
            table.addCell(c1);
            c1 = new PdfPCell(new Phrase(""+total));
            c1.setHorizontalAlignment(0);
            table.addCell(c1);
            
        }catch(Exception e){}
        subPara.add(table);

    }

     public void addFooterPage(Document document)throws DocumentException {
        Paragraph preface = new Paragraph();
        addEmptyLine(preface,3);
        preface.add(new Paragraph("PAID BY: " +"" +paidBy,subFont ));
        
        addEmptyLine(preface,6);
        addTab(preface,7);
        preface.add("_______________");
        preface.add(new Paragraph("                                                                                             Secretary's sign" ,subFont ));
        document.add(preface);
    } 
    public static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
    public static void addTab(Paragraph paragraph,int number){
        String tab="    ";
        Chunk chunk=new Chunk("");
        for(int i=0;i<number;i++){
            chunk.append(tab);
            paragraph.add(chunk);
        }
    }
}